plugins {
	`kotlin-dsl`
}

group = "conventions"

dependencies {
	implementation(libs.gradle.kotlin)
}
